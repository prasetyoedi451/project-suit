import React from 'react';
import Header from './components/Header';
import './index.css';
import Banner from './components/Banner';
import ListPost from './components/ListPost';

const App = () => {
  return (
    <div>
      <Header />
      <Banner imageUrl="/logo1.png" title="Ideas" description="Where all our great things begin" />
      <ListPost />
      <main>
        <section id="work" className="min-h-screen">Work Content</section>
        <section id="about" className="min-h-screen">About Content</section>
        <section id="services" className="min-h-screen">Services Content</section>
        <section id="ideas" className="min-h-screen">Ideas Content</section>
        <section id="careers" className="min-h-screen">Careers Content</section>
        <section id="contact" className="min-h-screen">Contact Content</section>
      </main>
    </div>
  );
};

export default App;

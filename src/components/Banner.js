import React, { useEffect, useRef } from 'react';

const Banner = ({ imageUrl, title, description }) => {
    const bannerRef = useRef(null);

    useEffect(() => {
        const handleScroll = () => {
            if (bannerRef.current) {
                const scrollPosition = window.scrollY;
                bannerRef.current.querySelector('.parallax-image').style.transform = `translateY(${scrollPosition * 0.5}px)`;
                bannerRef.current.querySelector('.parallax-text').style.transform = `translateY(${scrollPosition * 0.3}px)`;
            }
        };

        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    return (
        <div ref={bannerRef} className="relative h-screen flex items-center justify-center overflow-hidden">
            <img
                className="absolute inset-0 w-full h-full object-cover object-center opacity-50 parallax-image"
                src={imageUrl}
                alt="Banner Background"
            />
            <div className="absolute inset-0 bg-gray-900 bg-opacity-50"></div>
            <div className="text-center z-10 relative text-white parallax-text">
                <h1 className="text-4xl font-bold">{title}</h1>
                <p className="mt-4 text-lg">{description}</p>
            </div>
        </div>
    );
};

export default Banner;

import React, { useState, useEffect } from 'react';

const Header = () => {
    const [isHidden, setIsHidden] = useState(false);
    const [activeSection, setActiveSection] = useState('');
    const [lastScrollTop, setLastScrollTop] = useState(0);
    const [isMenuOpen, setIsMenuOpen] = useState(false);

    const handleScroll = () => {
        const currentScrollTop = window.pageYOffset || document.documentElement.scrollTop;

        if (currentScrollTop > lastScrollTop) {
            setIsHidden(true);
        } else {
            setIsHidden(false);
        }
        setLastScrollTop(currentScrollTop);

        const sections = document.querySelectorAll('main section');
        sections.forEach((section) => {
            const rect = section.getBoundingClientRect();
            if (rect.top <= 50 && rect.bottom >= 50) {
                setActiveSection(section.id);
            }
        });
    };

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, [lastScrollTop]);

    return (
        <header className={`fixed w-full top-0 left-0 bg-opacity-80 backdrop-filter backdrop-blur-lg transition-transform transform ${isHidden ? '-translate-y-full' : 'translate-y-0'} z-50`} style={{ backgroundColor: '#ed6a32' }}>
            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <div className="flex justify-between items-center h-16">
                    <div className="flex-shrink-0">
                        <img className="h-16 w-h-16" src="logo.png" alt="Logo" />
                    </div>
                    <div className="hidden md:block">
                        <nav className="ml-10 flex items-baseline space-x-4">
                            <a href="#work" className={`nav-link ${activeSection === 'work' ? 'text-white underline' : 'text-white'} hover:text-white`}>Work</a>
                            <a href="#about" className={`nav-link ${activeSection === 'about' ? 'text-white underline' : 'text-white'} hover:text-white`}>About</a>
                            <a href="#services" className={`nav-link ${activeSection === 'services' ? 'text-white underline' : 'text-white'} hover:text-white`}>Services</a>
                            <a href="#ideas" className={`nav-link ${activeSection === 'ideas' ? 'text-white underline' : 'text-white'} hover:text-white`}>Ideas</a>
                            <a href="#careers" className={`nav-link ${activeSection === 'careers' ? 'text-white underline' : 'text-white'} hover:text-white`}>Careers</a>
                            <a href="#contact" className={`nav-link ${activeSection === 'contact' ? 'text-white underline' : 'text-white'} hover:text-white`}>Contact</a>
                        </nav>
                    </div>
                    <div className="-mr-2 flex md:hidden">
                        <button onClick={() => setIsMenuOpen(!isMenuOpen)} type="button" className="inline-flex items-center justify-center p-2 rounded-md text-gray-700 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white" aria-controls="mobile-menu" aria-expanded="false">
                            <span className="sr-only">Open main menu</span>
                            {!isMenuOpen ? (
                                <svg className="block h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16m-7 6h7" />
                                </svg>
                            ) : (
                                <svg className="block h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            )}
                        </button>
                    </div>
                </div>
            </div>

            <div className={`md:hidden ${isMenuOpen ? 'block' : 'hidden'}`} id="mobile-menu">
                <div className="px-2 pt-2 pb-3 space-y-1 sm:px-3">
                    <a href="#work" className={`block px-3 py-2 rounded-md text-base font-medium ${activeSection === 'work' ? 'text-blue-500' : 'text-gray-700'} hover:text-blue-500`}>Work</a>
                    <a href="#about" className={`block px-3 py-2 rounded-md text-base font-medium ${activeSection === 'about' ? 'text-blue-500' : 'text-gray-700'} hover:text-blue-500`}>About</a>
                    <a href="#services" className={`block px-3 py-2 rounded-md text-base font-medium ${activeSection === 'services' ? 'text-blue-500' : 'text-gray-700'} hover:text-blue-500`}>Services</a>
                    <a href="#ideas" className={`block px-3 py-2 rounded-md text-base font-medium ${activeSection === 'ideas' ? 'text-blue-500' : 'text-gray-700'} hover:text-blue-500`}>Ideas</a>
                    <a href="#careers" className={`block px-3 py-2 rounded-md text-base font-medium ${activeSection === 'careers' ? 'text-blue-500' : 'text-gray-700'} hover:text-blue-500`}>Careers</a>
                    <a href="#contact" className={`block px-3 py-2 rounded-md text-base font-medium ${activeSection === 'contact' ? 'text-blue-500' : 'text-gray-700'} hover:text-blue-500`}>Contact</a>
                </div>
            </div>
        </header>
    );
};

export default Header;

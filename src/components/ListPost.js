import React, { useState, useEffect } from 'react';
import axios from 'axios';

function Card({ post }) {
    return (
        <div className="bg-white shadow-md rounded px-4 py-4">
            {/* <img src={post.small_image} alt={post.title} className="w-full h-48 object-cover rounded-t" /> */}
            <img src={post.small_image} className="w-full h-48 object-cover rounded-t" />

            <div className="px-4 py-2">
                <p className="text-gray-600">{post.updated_at}</p>
                <h2 className="text-lg font-bold">{post.title}</h2>
            </div>
        </div>
    );
}

function App() {
    const [posts, setPosts] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [itemsPerPage, setItemsPerPage] = useState(10);
    const [sort, setSort] = useState('-published_at');
    const [loading, setLoading] = useState(false);
    const [totalItems, setTotalItems] = useState(0);

    useEffect(() => {
        const fetchPosts = async () => {
            setLoading(true);
            try {
                const response = await axios.get('https://suitmedia-backend.suitdev.com/api/ideas', {
                    params: {
                        page: { number: currentPage, size: itemsPerPage },
                        append: ['small_image', 'medium_image'],
                        sort,
                    },
                });

                setPosts(response.data.data);
                setTotalItems(response.data.meta.total);
            } catch (error) {
                console.error(error);
            } finally {
                setLoading(false);
            }
        };
        fetchPosts();
    }, [currentPage, itemsPerPage, sort]);

    const handleSortChange = (newSort) => {
        setSort(newSort);
        setCurrentPage(1);
    };

    const handleItemsPerPageChange = (newItemsPerPage) => {
        setItemsPerPage(newItemsPerPage);
        setCurrentPage(1);
    };

    const handlePageChange = (newPage) => {
        setCurrentPage(newPage);
    };

    const startIndex = (currentPage - 1) * itemsPerPage + 1;
    const endIndex = Math.min(startIndex + itemsPerPage - 1, totalItems);

    return (
        <div className="container mx-auto p-4">
            <div className="flex flex-wrap">
                <div className="w-full md:w-2/12">Showing {startIndex} - {endIndex} of {totalItems}</div>
                <div className="w-full md:w-6/12"></div>
                <div className="w-full md:w-2/12"><label className="mr-2">Show per page:</label>
                    <select
                        value={itemsPerPage}
                        onChange={(e) => handleItemsPerPageChange(e.target.value)}
                        className="bg-white border border-gray-300 rounded py-2 px-4"
                    >
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                    </select></div>
                <div className="w-full md:w-2/12"><label className="mr-2">Sort by:</label>
                    <select
                        value={sort}
                        onChange={(e) => handleSortChange(e.target.value)}
                        className="bg-white border border-gray-300 rounded py-2 px-4"
                    >
                        <option value="-published_at">Newest</option>
                        <option value="published_at">Oldest</option>
                    </select></div>
            </div>

            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
                {loading ? (
                    <p>Loading...</p>
                ) : (
                    posts.map((post) => (
                        <Card key={post.id} post={post} />
                    ))
                )}
            </div>
            <div className="flex justify-center mb-4">
                <button onClick={() => handlePageChange(currentPage - 1)} className="bg-white border border-gray-300 rounded py-2 px-4">
                    Previous
                </button>
                <span className="mx-2">{`Page ${currentPage} of ${Math.ceil(totalItems / itemsPerPage)}`}</span>
                <button onClick={() => handlePageChange(currentPage + 1)} className="bg-white border border-gray-300 rounded py-2 px-4">
                    Next
                </button>
            </div>
        </div>
    );
}

export default App;